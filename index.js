let number = Number(prompt("Please give an input number more than 70"));
console.log("The number you provided is: "+number)

for (let i = number; i >= 0; i--){

	if(i <= 50){
		console.log("Current value is 50. Terminating the loop.")
		break;
	}

	if(i % 10 === 0){
		console.log("Current value is divisible by 10. Skipping the number.");
		continue;
	}

	if(i % 5 === 0){
		console.log(i);
	}
}


let myString = 'supercalifragilisticexpialidocious';
console.log(myString)

let myConsonants = '';

for(let i = 0; i < myString.length; i++){
	if (myString[i].toLowerCase() === 'a' || myString[i].toLowerCase() === 'e' || myString[i].toLowerCase() === 'i' || myString[i].toLowerCase() === 'o' || myString[i].toLowerCase() === 'u'){
		continue;
	}
	else{
		myConsonants += myString[i];
	}
}

console.log(myConsonants)


